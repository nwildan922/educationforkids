import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
@Component({
  selector: 'app-score',
  templateUrl: 'score.page.html',
  styleUrls: ['score.page.scss']
})
export class ScorePage {
  score:string;
  returnUrl:string;
  constructor(private navCtrl: NavController,private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.score = params.value;
      this.returnUrl = params.returnUrl;
      this.playSound();
    });
    
  };
  public onRetry(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          isExam : true
      }
    };
    this.navCtrl.navigateForward(this.returnUrl,navigationExtras);
  }
  public onExit(){
    this.navCtrl.navigateForward(`tabs/mainmenu`);
  }
  public playSound (){
    console.log('hit');
    let filePath = `assets/misc/sounds/win2.mp3`;
    var sound = new Audio(filePath);
    sound.play();  
  }
}
