import { Component } from '@angular/core';

@Component({
  selector: 'app-vegetable',
  templateUrl: 'vegetable.page.html',
  styleUrls: ['vegetable.page.scss']
})
export class VegetablePage {

  constructor() {
    
  }
  public playSound (type){
    let filePath = `assets/vegetable/sounds/${type}.m4a`;
    var sound = new Audio(filePath);
    sound.play();  
  }
}
