import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VegetablePage } from './vegetable.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { VegetablePageRoutingModule } from './vegetable-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    VegetablePageRoutingModule
  ],
  declarations: [VegetablePage]
})
export class VegetablePageModule {}
