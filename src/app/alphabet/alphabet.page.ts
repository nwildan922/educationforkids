import { Component } from '@angular/core';

@Component({
  selector: 'app-alphabet',
  templateUrl: 'alphabet.page.html',
  styleUrls: ['alphabet.page.scss']
})
export class AlphabetPage {

  constructor() {}
  public playSound (alphabet){
    let filePath = `assets/alphabet/sounds/${alphabet}.mp3`;
    var sound = new Audio(filePath);
    sound.play();  
  }
}
