import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AlphabetPage } from './alphabet.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { AlphabetPageRoutingModule } from './alphabet-routing.module'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: AlphabetPage }]),
    AlphabetPageRoutingModule,
  ],
  declarations: [AlphabetPage]
})
export class AlphabetPageModule {}
