import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-mainmenu',
  templateUrl: 'mainmenu.page.html',
  styleUrls: ['mainmenu.page.scss']
})
export class MainMenuPage {

  constructor(private navCtrl: NavController) {

  }
  public onGoTo(menu){
    this.navCtrl.navigateForward(`tabs/${menu}`);
  }
  public onLogout(){
    this.navCtrl.navigateForward(`startup`);  
  }
}
