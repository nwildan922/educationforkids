import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MainMenuPage } from './mainmenu.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { MainMenuPageRoutingModule } from './mainmenu-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    MainMenuPageRoutingModule
  ],
  declarations: [MainMenuPage]
})
export class MainMenuPageModule {}
