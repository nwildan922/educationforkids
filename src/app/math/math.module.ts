import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MathPage } from './math.page';
import { MathPageRoutingModule } from './math-routing.module'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: MathPage }]),
    MathPageRoutingModule,
  ],
  declarations: [MathPage]
})
export class MathPageModule {}
