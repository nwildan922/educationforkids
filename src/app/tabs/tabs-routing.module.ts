import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { StartupPage } from '../startup/startup.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'mainmenu',
        loadChildren: () => import('../main-menu/mainmenu.module').then(m => m.MainMenuPageModule)
      },
      {
        path: 'alphabet',
        loadChildren: () => import('../alphabet/alphabet.module').then(m => m.AlphabetPageModule)
      },
      {
        path: 'number',
        loadChildren: () => import('../number/number.module').then(m => m.NumberPageModule)
      },
      {
        path: 'fruit',
        loadChildren: () => import('../fruit/fruit.module').then(m => m.FruitPageModule)
      },
      {
        path: 'vegetable',
        loadChildren: () => import('../vegetable/vegetable.module').then(m => m.VegetablePageModule)
      },
      {
        path: 'math',
        loadChildren: () => import('../math/math.module').then(m => m.MathPageModule)
      },
      {
        path: 'reading',
        loadChildren: () => import('../reading/reading.module').then(m => m.ReadingPageModule)
      },
      {
        path: 'writing',
        loadChildren: () => import('../writing/writing.module').then(m => m.WritingPageModule)
      },
      {
        path: 'score',
        loadChildren: () => import('../score/score.module').then(m => m.ScorePageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/startup',
    pathMatch: 'full'
  },
  {
    path: 'startup',
    component: StartupPage,
    children: [
      {
        path: '',
        loadChildren: () => import('../startup/startup.module').then(m => m.StartupPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
