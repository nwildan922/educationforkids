import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-startup',
  templateUrl: 'startup.page.html',
  styleUrls: ['startup.page.scss']
})
export class StartupPage {

  constructor(private navCtrl: NavController) {
    //this.playSound();
  }
  public onStart(){
    this.navCtrl.navigateForward('tabs/mainmenu');
  }
  public playSound (){
    let filePath = `assets/misc/sounds/intro.mp3`;
    var sound = new Audio(filePath);
    sound.play();  
  }
}
