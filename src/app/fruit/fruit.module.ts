import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FruitPage } from './fruit.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { FruitPageRoutingModule } from './fruit-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    FruitPageRoutingModule
  ],
  declarations: [FruitPage]
})
export class FruitPageModule {}
