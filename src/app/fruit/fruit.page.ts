import { Component } from '@angular/core';

@Component({
  selector: 'app-fruit',
  templateUrl: 'fruit.page.html',
  styleUrls: ['fruit.page.scss']
})
export class FruitPage {

  constructor() {
    
  }
  public playSound (number){
    let filePath = `assets/fruit/sounds/${number}.mp3`;
    var sound = new Audio(filePath);
    sound.play();  
  }
}
