import { Component } from '@angular/core';

@Component({
  selector: 'app-number',
  templateUrl: 'number.page.html',
  styleUrls: ['number.page.scss']
})
export class NumberPage {

  constructor() {
    
  }
  public playSound (number){
    let filePath = `assets/number/sounds/${number}.mp3`;
    var sound = new Audio(filePath);
    sound.play();  
  }
}
