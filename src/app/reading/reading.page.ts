import { Component } from '@angular/core';
import content from "./content.json";
import { NavController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-reading',
  templateUrl: 'reading.page.html',
  styleUrls: ['reading.page.scss']
})
export class ReadingPage {
  public activePage;
  public listContent;
  public listExam;
  public page;
  public question;
  public score;
  public totalScore;
  public onStillExam;
  constructor(private navCtrl: NavController,private route: ActivatedRoute) {
    this.init();
  }
  private init(){
    this.onStillExam = false;
    this.listContent = content.listContent;
    this.checkMenuState();
    this.randomExamQuestion();
    this.clearExamData();
  }
  private compare( a, b ) {
    if ( a.sequence < b.sequence ){
      return -1;
    }
    if ( a.sequence > b.sequence ){
      return 1;
    }
    return 0;
  }
  private randomExamQuestion(){
    let temp = content.listExam;
    for(let i=0;i<temp.length;i++){
      temp[i].sequence = Math.floor(10 + Math.random() * 90).toString();
    }
    this.listExam = content.listExam.sort(this.compare);
    this.question = this.listExam[0];
  }
  private checkMenuState(){
    let isExam = false;
    this.route.queryParams.subscribe(params => {
      if ("undefined" === typeof (params["isExam"])) {
        this.activePage = 'learn';
      } else {
        isExam = params.isExam;      
        if(isExam){
          this.activePage = 'exam';
          this.randomExamQuestion();
          this.clearExamData();
        }else{
          this.activePage = 'learn';
        }
      }

      
    });
  }

  private next(){
    if(this.page == (this.listExam.length - 1)){
      this.onStillExam = false;
      this.calculateScore();
      this.showScore();
      this.clearExamData();
    }else{
      this.onStillExam = true;
      this.page += 1;
      this.question = this.listExam[this.page];
    }
  }
  private showScore(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          value : this.totalScore,
          returnUrl : 'tabs/reading'
      }
    };
    this.navCtrl.navigateForward(`tabs/score`,navigationExtras);
  }
  private calculateScore(){
    const length = this.score.length;
    let temp = 0;
    for(let i=0; i < length; i++){
      temp += this.score[i];
    }; 
    this.totalScore = Math.round((temp / length) * 100);
  }
  private clearExamData(){
    this.score = [];
    this.totalScore = 0;
    this.question = this.listExam[0];
    this.page = 0;
  }

  public playSound (soundId){
    console.log('sound ',soundId);
    let filePath = `assets/reading/sounds/${soundId}.mp3`;
    var sound = new Audio(filePath);
    sound.play();  
  }
  public onAnswer(selectedOption){
    if(selectedOption === this.question.answer ){
      this.score.push(1);
    }else{
      this.score.push(0);
    }
    this.next();
  }

  public onSwitchPage(page){
    if(page === 'exam' && this.onStillExam){      
      this.randomExamQuestion();      
      this.clearExamData();
    }
  }
}
