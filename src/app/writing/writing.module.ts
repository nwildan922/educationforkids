import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WritingPage } from './writing.page';
import { WritingPageRoutingModule } from './writing-routing.module'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: WritingPage }]),
    WritingPageRoutingModule,
  ],
  declarations: [WritingPage]
})
export class WritingPageModule {}
